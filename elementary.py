from nand2hk.atoms import nand


class Circuit(object):

    def __init__(self):
        self.nand_count = 0

    def nand(self, *inputs):
        self.nand_count += 1
        return nand(*inputs)

    def __call__(self, *inputs):
        return getattr(self, self.main_method)(*inputs)


class NotMixin(object):

    def not_(self, *inputs):
        return self.nand(inputs[0], inputs[0])

class AndMixin(object):

    def and_(self, *inputs):
        return self.not_(self.nand(inputs[0], inputs[1]))


class OrMixin(object):

    def or_(self, *inputs):
        return self.not_(self.and_(self.not_(inputs[0]), self.not_(inputs[1])))


class NotCircuit(Circuit, NotMixin):
    main_method = 'not_'


class AndCircuit(Circuit, NotMixin, AndMixin):
    main_method = 'and_'


class OrCircuit(Circuit, NotMixin, AndMixin, OrMixin):
    main_method = 'or_'
