class Nand:

    def __call__(self, *inputs):
        dic = {
            (0, 0): 1,
            (1, 0): 1,
            (0, 1): 1,
            (1, 1): 0,
        }
        return dic[inputs]

nand = Nand()
