from nand2hk.tables import Table
from nand2hk.elementary import NotCircuit, AndCircuit, OrCircuit


def test_not():
    nottable = Table('tests/tables/basics/elementary/Not.cmp', input_cols=['in'], output_cols=['out'])
    notcircuit = NotCircuit()
    for inputs in nottable.table:
        assert notcircuit(*inputs) == nottable(*inputs)

def test_and():
    andtable = Table('tests/tables/basics/elementary/And.cmp', input_cols=['a', 'b'], output_cols=['out'])
    andcircuit = AndCircuit()
    for inputs in andtable.table:
        assert andcircuit(*inputs) == andtable(*inputs)


def test_or():
    ortable = Table('tests/tables/basics/elementary/Or.cmp', input_cols=['a', 'b'], output_cols=['out'])
    orcircuit = OrCircuit()
    for inputs in ortable.table:
        assert orcircuit(*inputs) == ortable(*inputs)
