class Table(object):

    def __init__(self, file_path, input_cols, output_cols):
        self.path = file_path
        self.input_cols = input_cols
        self.output_cols = output_cols
        self.table = {}
        with open(file_path) as f:
            stringa = f.read()
            lines = stringa.split('\n')
            for line in [i for i in lines[1:] if i != '']:
                cols = tuple(int(c) for c in line.split('|')[1:-1])
                io_split = len(input_cols)
                if len(output_cols) == 1:
                    output = cols[io_split:][0]
                else:
                    output = cols[io_split:]
                self.table[cols[:io_split]] = output

    def __iter__(self):
        return self.table.keys()

    def __call__(self, *inputs):
        return self.table[inputs]
